import json

import requests

from real_time_currency_converter.parser import Parser

TIMEOUT = 30


class Scraper:
    api_hostname = "https://api.exchangerate-api.com/"
    api_searchpath = "v4/latest/USD"
    header_param = {'Accept': 'application/json'}

    def __init__(self, parser=Parser()):
        self.data = None
        self.parser = parser

    def get_response(self, url: str, query: dict):
        """

        :param url:
        :param query:
        :return:
        """
        if not isinstance(url, str) or not isinstance(query, dict):
            raise TypeError

        try:
            response = requests.get(url=url, params=query,
                                    headers=self.header_param)
            response.raise_for_status()
            return response
        except requests.exceptions.HTTPError as errh:
            print("Http Error:", errh)
        except requests.exceptions.ConnectionError as errc:
            print("Error Connecting:", errc)
        except requests.exceptions.Timeout as errt:
            print("Timeout Error:", errt)
        except requests.exceptions.RequestException as err:
            print("OOps: Something Else", err)

    def fetch_conversion_rates(self) -> dict:
        """

        :return:
        """
        url = f'{self.api_hostname}{self.api_searchpath}?'
        query = {}
        response = self.get_response(url=url, query=query)
        return json.loads(response.content)

    def parse_data_to_currencies(self, data: dict) -> dict:
        """

        :param data:
        :return:
        """
        if not isinstance(data, dict):
            raise TypeError

        return self.parser.get_currencies(data)

    def fetch_historical_data(self):
        url = f'{self.api_hostname}{self.api_searchpath}?'
        query = {}
        response = self.get_response(url=url, query=query)
        return json.loads(response.content)


if __name__ == '__main__':
    scraper = Scraper()
    # data = scraper.fetch_conversion_rates()
    # scraper.parse_data_to_currencies(data=data)
