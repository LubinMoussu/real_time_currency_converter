import tkinter as tk
from tkinter import ttk, mainloop, Button, Label, Entry, StringVar
import re
from real_time_currency_converter.currency_converter import CurrencyConverter


class CurrencyConverterUI(tk.Tk):
    def __init__(self, converter=CurrencyConverter()):
        tk.Tk.__init__(self)
        self.convert_button = Button(self, text="Convert", fg="black", command=self.perform)
        self.currency_converter = converter
        self.date_label = Label(self,
                                text=f"1 Indian Rupee equals = {self.currency_converter.convert('INR', 'USD', 1)} USD \n Date : {self.currency_converter.data['date']}",
                                relief=tk.GROOVE, borderwidth=5)
        self.intro_label = Label(self, text='Real Time Currency Convertor', fg='blue', relief=tk.RAISED,
                                 borderwidth=3)

        self.initialize_user_interface()

    def initialize_user_interface(self):
        self.title = 'Currency Converter'
        self.geometry("500x200")

        # Label
        self.intro_label.config(font=('Courier', 15, 'bold'))
        self.intro_label.place(x=10, y=5)
        self.date_label.place(x=160, y=50)

        self.initialize_entry_box()
        self.initialize_dropdown()
        self.placing()
        self.initialize_convert_button()

    def initialize_entry_box(self):
        valid = (self.register(self.restrict_number_only), '%d', '%P')
        self.amount_field = Entry(self, bd=3, relief=tk.RIDGE, justify=tk.CENTER, validate='key', validatecommand=valid)
        self.converted_amount_field_label = Label(self, text='', fg='black', bg='white', relief=tk.RIDGE,
                                                  justify=tk.CENTER, width=17, borderwidth=3)

    def initialize_dropdown(self):
        self.from_currency_variable = StringVar(self)
        self.from_currency_variable.set("INR")  # default value
        self.to_currency_variable = StringVar(self)
        self.to_currency_variable.set("USD")  # default value

        font = ("Courier", 12, "bold")
        self.option_add('*TCombobox*Listbox.font', font)
        self.from_currency_dropdown = ttk.Combobox(self, textvariable=self.from_currency_variable,
                                                   values=list(self.currency_converter.currencies.keys()), font=font,
                                                   state='readonly', width=12, justify=tk.CENTER)
        self.to_currency_dropdown = ttk.Combobox(self, textvariable=self.to_currency_variable,
                                                 values=list(self.currency_converter.currencies.keys()), font=font,
                                                 state='readonly', width=12, justify=tk.CENTER)

    def placing(self):
        self.from_currency_dropdown.place(x=30, y=120)
        self.amount_field.place(x=36, y=150)
        self.to_currency_dropdown.place(x=340, y=120)
        # self.converted_amount_field.place(x = 346, y = 150)
        self.converted_amount_field_label.place(x=346, y=150)

    def initialize_convert_button(self):
        self.convert_button.config(font=('Courier', 10, 'bold'))
        self.convert_button.place(x=225, y=135)

    def perform(self):
        amount = float(self.amount_field.get())
        from_curr = self.from_currency_variable.get()
        to_curr = self.to_currency_variable.get()

        converted_amount = self.currency_converter.convert(from_curr, to_curr, amount)
        converted_amount = round(converted_amount, 2)

        self.converted_amount_field_label.config(text=str(converted_amount))

    @staticmethod
    def restrict_number_only(action, string):
        regex = re.compile(r"[0-9,]*?(\.)?[0-9,]*$")
        result = regex.match(string)
        return string == "" or (string.count('.') <= 1 and result is not None)


if __name__ == '__main__':
    converter = CurrencyConverter()
    converter.fetch_currencies_workflow()
    converter_ui = CurrencyConverterUI(converter=converter)
    mainloop()
