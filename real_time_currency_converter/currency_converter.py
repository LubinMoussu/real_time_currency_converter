from real_time_currency_converter.scraper import Scraper


class CurrencyConverter:
    round_precision = 4

    def __init__(self, scraper=Scraper()):
        self.scraper = scraper
        self.data = None
        self.currencies = None
        self.date = None

    def get_data(self):
        self.data = self.scraper.fetch_conversion_rates()

    def get_currencies(self):
        """

        :return:
        """
        self.currencies = self.scraper.parse_data_to_currencies(self.data)

    def get_date(self):
        self.date = self.data['date']

    def convert(self, from_currency, to_currency, amount):
        """

        :param from_currency:
        :param to_currency:
        :param amount:
        :return:
        """
        if not isinstance(from_currency, str) or not isinstance(to_currency, str):
            raise TypeError

        if from_currency != 'USD':
            amount = amount / self.currencies[from_currency]

        # limiting the precision to 4 decimal places
        return round(amount * self.currencies[to_currency], self.round_precision)

    def fetch_currencies_workflow(self):
        self.get_data()
        self.get_currencies()


if __name__ == '__main__':
    converter = CurrencyConverter()
    converter.get_data()
    converter.get_currencies()
    print(converter.currencies)
    print(converter.convert("USD", "INR", 100))
