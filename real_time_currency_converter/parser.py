class Parser:
    def __init__(self):
        pass

    @staticmethod
    def get_currencies(data: dict):
        """

        :param data:
        :return:
        """
        if not isinstance(data, dict):
            raise TypeError
        return data["rates"]
