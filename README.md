# Real Time Currency Converter
GUI based simple Real-time currency convertor (Using exchangerate API) which can convert amounts from one currency to another currency.

## Documentation
contains:
- Scraper using exchangerate API (https://www.exchangerate-api.com/)
- Parser to parse API call content
- CurrencyConverter to convert
- CurrencyConverterUI to create the main window with widgets
## Installation

Install dependencies
```bash
 [python_path]python -m pip install -r requirements.txt
```

## Usage/Examples

```bash
 [python_path]python app.py
```
