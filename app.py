from tkinter import mainloop

from real_time_currency_converter.currency_converter import CurrencyConverter
from real_time_currency_converter.currency_converter_ui import CurrencyConverterUI


def run_converter():
    converter = CurrencyConverter()
    converter.fetch_currencies_workflow()
    converter_ui = CurrencyConverterUI(converter=converter)
    mainloop()


if __name__ == '__main__':
    run_converter()
